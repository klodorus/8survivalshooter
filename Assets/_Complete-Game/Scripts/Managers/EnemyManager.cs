﻿using UnityEngine;

namespace CompleteProject
{
    public class EnemyManager : MonoBehaviour
    {
        public PlayerHealth playerHealth;       // Reference to the player's heatlh.
        public GameObject enemy;                // The enemy prefab to be spawned.
        public float spawnTime = 3f;            // How long between each spawn.
        public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.

        [SerializeField]
        MonoBehaviour factory;
        IFactory Factory { get { return factory as IFactory; } }

        void Start ()
        {
            //Mengeksekusi fungsi Spawn setiap beberapa detik sesuai dengan nilai spawnTime
        }


        void Spawn ()
        {
            //Jika player telah mati maka tidak membuat enemy baru
            if(playerHealth.currentHealth <= 0f)
            {
                // ... exit the function.
                return;
            }

            //Mendapatkan nilai random
            int spawnPointIndex = Random.Range (0, spawnPoints.Length);
            int spawnEnemy = Random.Range(0, 3);

            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
            // Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

            //Menduplikasi enemy
            Factory.FactoryMethod(spawnEnemy);
        }
    }
}